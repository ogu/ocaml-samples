let bisection ?(tol=1.0e-5) ?(max_iter=100) ~low ~high f =
  let rec loop num_iter a b =
    if num_iter = 0 then None
    else begin
      let c = 0.5 *. (a +. b) in
      let f_c = f c in
      if f_c = 0. || (b -. a < 2. *. tol) then
        Some c
      else if f a *. f_c > 0. then
        loop (num_iter-1) c b
      else
        loop (num_iter-1) a c
    end
  in
  loop max_iter low high
