open Core

module List = struct
  include List

  let pairwise l =
    let rec loop acc l = match l with
      | [] | [_]      -> List.rev acc
      | x :: [y]      -> loop ((x, y) :: acc) []
      | x :: y :: [z] -> loop ((y, z) :: (x, y) :: acc) []
      | x :: y :: l'  -> loop ((x, y) :: acc) (y :: l')
    in
    loop [] l

  let average_by l ~f =
    let sum = List.fold l ~init:0. ~f:(fun sum x -> sum +. f x) in
    let len = List.length l |> Float.of_int in
    sum /. len

  let average l = average_by l ~f:Fn.id
end
