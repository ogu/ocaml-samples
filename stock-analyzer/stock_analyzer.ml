open Core
open Async
open Utils

module Ascii_table = Textutils.Ascii_table

module Analyzer : sig
  type t  (* A stock analyzer *)

  val create : prices:float list -> days:int -> t

  val return  : t -> float
  val std_dev : t -> float

end = struct
  type t = { prices : float list; days : int }

  let create ~prices ~days =
    { prices = List.take prices days; days }

  let return t =
    let start_price = List.nth_exn t.prices (t.days - 1) in
    let last_price = List.nth_exn t.prices 0 in
    last_price /. start_price -. 1.

  let std_dev t =
    let log_rets =
      t.prices
      |> List.pairwise
      |> List.map ~f:(fun (x, y) -> log (x /. y))
    in
    let mean = log_rets |> List.average in
    let sqr x = x *. x in
    let var = log_rets |> List.average_by ~f:(fun r -> sqr (r -. mean)) in
    sqrt (var *. float t.days)
end

let query_uri ticker =
  let base_uri = Uri.of_string (String.concat [
    "http://ichart.finance.yahoo.com/table.csv?s=";ticker;"&ignore=.csv"])
  in
  Uri.add_query_param base_uri ("q", [ticker])

let get_prices_from_csv csv =
  let prices =
    String.split csv ~on:'\n'
    |> List.tl_exn
    |> List.map ~f:(fun line -> String.split line ~on:',' |> Array.of_list)
    |> List.filter ~f:(fun values -> values |> Array.length = 7)
    |> List.map ~f:(fun values -> Float.of_string values.(6))
  in
  prices

let load_prices ticker =
  try_with (fun () ->
    Cohttp_async.Client.get (query_uri ticker)
    >>= fun (_, body) ->
    Cohttp_async.Body.to_string body
    >>| fun csv ->
    ticker, get_prices_from_csv csv)
  >>| function
  | Ok (ticker, result) -> (ticker, Ok result)
  | Error _             -> (ticker, Error "Unexpected failure")

let print_table records =
  Ascii_table.(output ~oc:stdout [
    Column.create "Ticker"
      (fun (t, _, _) -> t);
    Column.create "Return" ~align:Align.Right
      (fun (_, r, _) -> sprintf "%2.2f%%" (r *. 100.));
    Column.create "StdDev" ~align:Align.Right
      (fun (_, _, s) -> sprintf "%2.2f%%" (s *. 100.));
  ] records)

let analyze_and_print tickers days =
  Deferred.List.map ~how:`Parallel tickers ~f:load_prices
  >>| fun results ->
  List.filter_map results ~f:(fun (ticker, result) ->
    match result with
    | Error e ->
      print_endline ("Query for "^ticker^" failed: "^e); None
    | Ok prices ->
      let a = Analyzer.create ~prices ~days in
      match Or_error.try_with (fun () -> Analyzer.(return a, std_dev a)) with
      | Error e ->
        printf "%s analyzer failed: %s\n" ticker (Error.to_string_hum e); None
      | Ok (return, std_dev) ->
        Some (ticker, return, std_dev)
  ) |> print_table

let () =
  Command.async
    ~summary:"Analyzes stock prices for the given tickers"
    Command.Spec.(
      empty
      +> anon (sequence ("ticker" %: string))
      +> flag "-days" (optional_with_default 252 int)
        ~doc:" Specify the number of days"
    )
    (fun tickers days () -> analyze_and_print tickers days)
  |> Command.run
