open Core

(* Topological sorting:
   http://stackoverflow.com/questions/4653914/topological-sort-in-ocaml *)

module Digraph : sig
  type 'a t

  val from_adj_lists : ('a * 'a list) list -> 'a t
  val topological_sort : 'a t -> 'a list
end = struct

  type 'a t = ('a, 'a list) Map.Poly.t

  exception Digraph_is_cyclic

  let from_adj_lists adj_lists = Map.Poly.of_alist_exn adj_lists

  let dfs t ~visited ~start =
    let rec traverse curr_path visited node =
      if Set.Poly.mem curr_path node then
        raise Digraph_is_cyclic
      else
        if List.mem visited node ~equal:(=) then
          visited
        else
          match Map.find t node with
          | None ->
            node :: visited
          | Some neighbors ->
            let new_path = Set.Poly.add curr_path node in
            let visited =
              List.fold neighbors ~init:visited ~f:(traverse new_path)
            in
            node :: visited
    in
    traverse Set.Poly.empty visited start

  let topological_sort t =
    Map.Poly.fold_right t ~init:[] ~f:(fun ~key:node ~data:_ visited ->
      dfs t ~visited ~start:node)
end

(* Example is taken from Cormen et al. *)
let dressing_order =
  Digraph.from_adj_lists [
    `Socks , [`Shoes];
    `Shorts, [`Pants; `Shoes];
    `Pants , [`Belt; `Shoes];
    `Belt  , [`Jacket];
    `Shirt , [`Belt; `Tie];
    `Tie   , [`Jacket];
    `Watch , [];
  ] |> Digraph.topological_sort

let _ = assert (
  dressing_order
  = [`Watch; `Shorts; `Pants; `Shirt; `Tie; `Socks; `Shoes; `Belt; `Jacket]
)
