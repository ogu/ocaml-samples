module Int = struct
  (* replace polymorphic compare *)
  let min (x : int) y = if x < y then x else y
  let max (x : int) y = if x > y then x else y
end

let find_min_max l = match l with
  | []  -> None
  | [x] -> Some (x,x)
  | x :: y :: l' ->
    let rec loop l min max = match l with
      | []  -> Some (min, max)
      | [x] -> Some (Int.min x min, Int.max x max)
      | x :: y :: xs ->
        if x < y
        then loop xs (Int.min x min) (Int.max y max)
        else loop xs (Int.min y min) (Int.max x max)
    in
    if x < y then loop l' x y else loop l' y x

let _ = assert (find_min_max [] = None)
let _ = assert (find_min_max [1] = Some (1, 1))
let _ = assert (find_min_max [2; 4; 3; 9; 5; 7; 8; 6; 1] = Some (1, 9))
