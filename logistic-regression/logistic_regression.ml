open Core
open Lacaml.D

(* OCaml translation of the F# implementation from
   http://mathias-brandewinder.github.com/Machine-Learning-In-Action *)

let one = Vec.of_array [|1.|]
let scalar_mul a v = Vec.mul (Vec.make (Vec.dim v) a) v

let predict weights input =
  let sigmoid x = 1. /. (1. +. exp (-.x)) in
  let input' = Vec.concat [one;input] in
  sigmoid (dot weights input')

let predict_all weights inputs =
  List.map inputs ~f:(fun input -> predict weights input)

let train training_set ~epsilon =
  let update alpha weights (input, label) =
    let error = label -. predict weights input in
    let input' = Vec.concat [one;input] in
    Vec.add weights (scalar_mul (alpha *. error) input')
  in
  let change_rate before after =
    let norm x = sqrt (Vec.sqr_nrm2 x) in
    let numer = norm (Vec.sub after before) in
    let denom = norm before in
    numer /. denom
  in
  (* We use stochastic gradient ascent. *)
  let rec stoch_gradient_ascent weights alpha =
    let update = update alpha in
    let new_weights = List.fold training_set ~init:weights ~f:update in
    if change_rate weights new_weights < epsilon then
      new_weights
    else
      stoch_gradient_ascent new_weights (0.9 *. alpha)
  in
  let num_features = Vec.dim (List.nth_exn training_set 0 |> fst) in
  let init_weights = Vec.make0 (num_features+1) in
  stoch_gradient_ascent init_weights 1.0

let calc_accuracy ~pred_labels ~real_labels =
  let l = List.length pred_labels in
  let num_correct =
    List.map2_exn pred_labels real_labels ~f:(-.)
    |> List.fold ~init:0 ~f:(fun count x -> count + if x = 0. then 1 else 0)
  in
  float num_correct /. float l

let classify x =  if x <= 0.5 then 0. else 1.

(* Simple example *)

let gen_training_set ~size =
  let w0, w1, w2, w3, w4 = 1., 2., 3., 4., -10. in
  let weights = Vec.of_array [|w0;w1;w2;w3;w4|] in
  let gen_input () = Vec.init 4 (fun _ -> Random.float 10.) in
  let inputs = List.init size (fun _ -> gen_input ()) in
  let labels = predict_all weights inputs |> List.map ~f:classify in
  inputs, labels

let () =
  print_endline "Generate dataset...";
  let inputs, real_labels = gen_training_set ~size:10_000 in
  print_endline "Train dataset...";
  let weights = train (List.zip_exn inputs real_labels) ~epsilon:(1e-4) in
  let pred_labels = predict_all weights inputs |> List.map ~f:classify in
  let accuracy = calc_accuracy ~pred_labels ~real_labels in
  printf "Correctly classified: %2.1f %%\n%!" (accuracy *. 100.)
