open Core

module List_ext : sig

  val unfold
    :  init:'state
    -> f:('state -> ('a * 'state) option)  (* generator *)
    -> 'a list

end = struct

  let unfold ~init ~f =
    let rec loop state acc =
      match f state with
      | None -> List.rev acc
      | Some (x, new_state) -> loop new_state (x :: acc)
    in
    loop init []

end

type tree = E | T of int * tree * tree

let tree =
  T (1, T (2, T (4, E, E), T (5, E, E)), T (3, T (6, E, E), E))

(* Functional level-order tree traversal. *)
let level_order_fun tree =
  List_ext.unfold ~init:[tree] ~f:(fun state ->
    match state with
    | [] -> None
    | h :: t ->
      match h with
      | E -> None
      | T (x, E, E) -> Some (x, t)
      | T (x, c, E)
      | T (x, E, c) -> Some (x, t @ [c])
      | T (x, a, b) -> Some (x, t @ [a;b]))

let _ = assert (level_order_fun E = [])
let _ = assert (level_order_fun (T (1, E, E)) = [1])
let _ = assert (level_order_fun tree = [1; 2; 3; 4; 5; 6])

(* Imperative level-order tree traversal. *)
let level_order_imp tree =
  let queue = Queue.singleton tree in
  let rec loop acc =
    match Queue.dequeue queue with
    | None -> List.rev acc
    | Some tree ->
      match tree with
      | E -> loop acc
      | T (x, E, E) -> loop (x :: acc)
      | T (x, c, E)
      | T (x, E, c) ->
        Queue.enqueue queue c;
        loop (x :: acc)
      | T (x, a, b) ->
        Queue.enqueue queue a;
        Queue.enqueue queue b;
        loop (x :: acc)
  in
  loop []

let _ = assert (level_order_imp E = [])
let _ = assert (level_order_imp (T (1, E, E)) = [1])
let _ = assert (level_order_imp tree = [1; 2; 3; 4; 5; 6])
