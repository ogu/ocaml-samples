open Core_kernel

(* European option pricing by monte carlo simulation. *)

let normal_cdf_inverse p =
  (* Abramowitz and Stegun formula 26.2.23 *)
  let rational_approx t =
    let c = [|2.515517; 0.802853; 0.010328|] in
    let d = [|1.432788; 0.189269; 0.001308|] in
    let numer = (c.(2)*.t +. c.(1))*.t +. c.(0) in
    let denom = ((d.(2)*.t +. d.(1))*.t +. d.(0))*.t +. 1. in
    t -. numer /. denom
  in
  if p < 0.5
  then -.rational_approx (sqrt (-.2.*.log p))
  else   rational_approx (sqrt (-.2.*.log (1.-.p)))
;;

(** [normal_gen ~mu ~sigma] returns a random generator for normal random
    variables with mean [mu] and standard deviation [sigma]. *)
let normal_gen ~mu ~sigma =
  stage (fun () ->
    let u = Random.float 1. in
    let z = normal_cdf_inverse u in
    mu +. sigma *. z
  )
;;

let option_price ~s0 ~r ~sigma ~t ~n ~payoff =
  let drift = (r -. 0.5 *. sigma**2.) *. t in
  let w = unstage (normal_gen ~mu:0. ~sigma:(sqrt t)) in
  let payoffs =
    Array.init n ~f:(fun _ -> w ())
    |> Array.map ~f:(fun w -> payoff (s0 *. exp (drift +. sigma *. w)))
  in
  let sum = Array.fold payoffs ~init:0. ~f:(+.) in
  exp (-.r *. t) *. (sum /. float n)
;;

let () =
  let s0 = 100. in     (* Price of underlying asset *)
  let k = 100. in      (* Strike price *)
  let r = 0.1 in       (* Risk-free interest rate *)
  let sigma = 0.25 in  (* Volatility *)
  let t = 1. in        (* Time to maturity *)
  let n = 100_000 in   (* Number of simulations to run *)
  let price_of right =
    let payoff s =
      match right with
      | `Call -> max (s -. k) 0.
      | `Put  -> max (k -. s) 0.
    in
    option_price ~s0 ~r ~sigma ~t ~n ~payoff
  in
  printf "Simulated call option price = %g\n%!" (price_of `Call);
  printf "Simulated put  option price = %g\n%!" (price_of `Put);
;;
