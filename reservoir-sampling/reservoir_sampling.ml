open Core

(* Reservoir sampling
   see http://en.wikipedia.org/wiki/Reservoir_sampling *)

let rand_select ~next sample_size =
  let sample = Array.create ~len:sample_size None in
  let rec loop num_seen =
    match next () with
    | None -> ()
    | Some item ->
      if num_seen < sample_size then
        sample.(num_seen) <- Some item
      else begin
        let r = Random.int num_seen in
        if r < sample_size then
          sample.(r) <- Some item
      end;
      loop (num_seen + 1)
  in
  loop 0;
  sample

let make_iterator l =
  let items = Array.of_list l in
  let num_items = Array.length items in
  let counter = ref 0 in
  stage (fun () ->
    let some_item =
      if !counter < num_items then
        Some items.(!counter)
      else None
    in
    incr counter;
    some_item)

let sample =
  let next = make_iterator (List.range 1 100) |> unstage in
  let sample_size = 10 in
  rand_select ~next sample_size
