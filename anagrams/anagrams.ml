(* Print largest set of anagrams from a dictionary. *)

open Core
open Async

let anagrams words =
  let module M = Map.Make (struct
    type t = char array [@@deriving sexp]
    let compare = Pervasives.compare
  end)
  in
  let anagrams = List.fold words ~init:M.empty ~f:(fun anagrams word ->
    let key = Array.init (String.length word) (String.get word) in
    Array.sort ~cmp:Char.compare key;
    let value = match M.find anagrams key with
      | None -> [word]
      | Some xs -> word :: xs
    in
    M.add anagrams ~key ~data:value)
  in
  M.fold anagrams ~init:[] ~f:(fun ~key:_ ~data:xs xss -> xs :: xss)

let _ = assert (
  anagrams ["mary"; "army"; "hello"; "olleh"]
  = [["olleh"; "hello"]; ["army"; "mary"]]
)

let find_longest_list xss =
  List.fold xss ~init:(0, None) ~f:(fun (cur_max, res) xs ->
    let len = List.length xs in
    if len > cur_max then len, Some xs else cur_max, res)
  |> Tuple.T2.get2

let _ = assert (find_longest_list [] = None)
let _ = assert (find_longest_list [[1;2];[1;2;3;4];[3]] = Some [1;2;3;4])

let dictionary = "/usr/share/dict/words" (* Path to dictionary *)

let lines file =
  Reader.with_file file ~f:(fun reader ->
    Reader.contents reader
    >>| fun contents ->
    String.split ~on:'\n' contents)

let () =
  upon (lines dictionary) (fun all_words ->
    let is_word s =
      let re = Str.regexp "^[a-z]+$" in
      Str.string_match re s 0
    in
    let words = List.filter all_words ~f:(fun s ->
      (String.length s >= 3) && is_word s)
    in
    printf "%d words\n" (List.length words);
    begin
      match find_longest_list (anagrams words) with
      | None ->
        prerr_endline "No anagrams were found!"
      | Some anagrams ->
        print_endline "Largest set of anagrams: ";
        print_endline (String.concat anagrams ~sep:" ");
    end;
    shutdown 0
  );
  never_returns (Scheduler.go ())
