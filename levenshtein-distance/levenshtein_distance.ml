open Core

module Mat : sig
  type 'a t

  val data : 'a t -> 'a array array

  val set : 'a t -> int -> int -> 'a -> unit
  val get : 'a t -> int -> int -> 'a

  val init
    :  ?basex:int -> ?basey:int
    -> dimx:int -> dimy:int
    -> f:(int -> int -> 'a)
    -> 'a t

  val iteri : 'a t -> f:(int -> int -> 'a -> unit) -> unit

end = struct
  type 'a t =
    { data : 'a array array;
      basex : int;
      basey : int;
      dimx : int;
      dimy : int;
    }

  let data t = t.data

  let set t i j x = t.data.(i-t.basex).(j-t.basey) <- x
  let get t i j = t.data.(i-t.basex).(j-t.basey)

  let init ?(basex=0) ?(basey=0) ~dimx ~dimy ~f =
    let init = f basex basey in
    let data = Array.make_matrix init ~dimx ~dimy in
    for i = 0 to dimx-1 do
      for j = 0 to dimy-1 do
        data.(i).(j) <- f (i+basex) (j+basey)
      done
    done;
    { data; basex; basey; dimx; dimy }

  let iteri t ~f =
    for i = 0 to t.dimx-1 do
      for j = 0 to t.dimy-1 do
        f (i+t.basex) (j+t.basey) t.data.(i).(j)
      done
    done
end

(* Levenshtein distance
   see http://en.wikipedia.org/wiki/Levenshtein_distance
   see http://apollo13cn.blogspot.de/2012/11/
   f-on-algorithms-wagnerfischer-algorithm.html *)
let levenshtein s1 s2 =
  let n1 = String.length s1 in
  let n2 = String.length s2 in
  let m = Mat.init ~basex:(-1) ~basey:(-1) ~dimx:(n1+1) ~dimy:(n2+1)
    ~f:(fun i j ->
      match i, j with
      | -1, -1 -> 0
      |  _, -1 -> i+1
      | -1,  _ -> j+1
      | _      -> 0)
  in
  let min x y z = Int.min x (Int.min y z) in
  Mat.iteri m ~f:(fun i j _ ->
    match i, j with
    | -1, -1
    |  _, -1
    | -1, _ -> ()
    | _ when s1.[i] = s2.[j] ->
      (* no operation required *)
      Mat.set m i j (Mat.get m (i-1) (j-1))
    | _ ->
      let del = Mat.get m (i-1) j in
      let ins = Mat.get m i (j-1) in
      let sub = Mat.get m (i-1) (j-1) in
      Mat.set m i j (1 + min del ins sub));
  Mat.data m

let _ =
  let s1 = "sitting" in
  let s2 = "kitten" in
  let d = levenshtein s1 s2 in
  assert (d = [|[|0; 1; 2; 3; 4; 5; 6|];
                [|1; 1; 2; 3; 4; 5; 6|];
                [|2; 2; 1; 2; 3; 4; 5|];
                [|3; 3; 2; 1; 2; 3; 4|];
                [|4; 4; 3; 2; 1; 2; 3|];
                [|5; 5; 4; 3; 2; 2; 3|];
                [|6; 6; 5; 4; 3; 3; 2|];
                [|7; 7; 6; 5; 4; 4; 3|]|]
  );
  printf "lev('%s', '%s') = %d\n" s1 s2 d.(7).(6)
