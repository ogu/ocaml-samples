(* Imperative version of Quicksort *)

let sort a =
  let swap i j =
    let temp = a.(i) in
    a.(i) <- a.(j);
    a.(j) <- temp
  in
  let rec sort_aux l r =
    let pivot = a.((l + r) / 2) in
    let i = ref l in
    let j = ref r in
    while !i <= !j do
      while a.(!i) < pivot do incr i done;
      while a.(!j) > pivot do decr j done;
      if !i <= !j then begin
        swap !i !j;
        incr i;
        decr j;
      end
    done;
    if !j > l then sort_aux l !j;
    if !j < r then sort_aux !i r;
  in
  if Array.length a > 0
  then sort_aux 0 (Array.length a - 1)
  else failwith "empty array"

let _ =
  let test = [|3;4;1;2;5;7;6;9;8|] in
  sort test;
  assert (test = [|1;2;3;4;5;6;7;8;9|])
