open Core

(* A continuation monad. *)
module Cont : sig
  type ('a, 'r) t
  include Monad.S2 with type ('a, 'r) t := ('a, 'r) t
  (* Scheme's call-with-current-continuation control operator *)
  val callcc : (('a -> ('b, 'r) t) -> ('a, 'r) t) -> ('a, 'r) t
  val run : ('r, 'r) t -> 'r
end = struct

  type ('a, 'r) _t = ('a -> 'r) -> 'r

  type ('a, 'r) t = ('a, 'r) _t

  include (Monad.Make2 (struct

    type ('a, 'r) t = ('a, 'r) _t

    let return x = fun k -> k x

    let bind t ~f = fun k -> t (fun v -> f v k)

    let map = `Custom (fun t ~f -> fun k -> t (Fn.compose k f))

  end) : Monad.S2 with type ('a, 'r) t := ('a, 'r) t)

  let callcc h =
    let throw c x = fun _k -> c x in
    fun k -> h (throw k) k

  let run t = t Fn.id
end
open Cont

type name = string

type value =
| Wrong
| Num of int
| Fun of (value -> (value, answer) Cont.t)
and answer = value

let show_value = function
  | Wrong -> "Wrong"
  | Num n -> Int.to_string n
  | Fun _ -> "<function>"

module Env = struct
  type t = (name, value) List.Assoc.t

  let empty = []

  let extend t name value =
    List.Assoc.add t name value ~equal:(=)

  let lookup t name =
    match List.Assoc.find t name ~equal:(=) with
    | Some v -> return v
    | None   -> return Wrong
end

type term =
| Var of name
| Con of int
| Add of term * term
| Lam of name * term
| App of term * term
| Ccc of name * term

let add a b = match a, b with
  | Num m, Num n -> return (Num (m + n))
  | _, _ -> return Wrong

let apply a b = match a with
  | Fun f -> f b
  | _ -> return Wrong

let rec interp term env = match term with
  | Var x ->
    Env.lookup env x
  | Con n ->
    return (Num n)
  | Add (u, v) ->
    interp u env >>= fun a ->
    interp v env >>= fun b ->
    add a b
  | Lam (x, u) ->
    return (Fun (fun a -> interp u (Env.extend env x a)))
  | App (u, v) ->
    interp u env >>= fun a ->
    interp v env >>= fun b ->
    apply a b
  | Ccc (x, u) ->
    callcc (fun k -> interp u (Env.extend env x (Fun k)))

let test t = show_value (Cont.run (interp t Env.empty))

let term0 = App (Con 1, Con 2)
let term1 = App (Lam ("x", Add (Var "x", Var "x")), Add (Con 10, Con 11))
let term2 = Add (Con 1, Ccc ("k", Add (Con 2, App (Var "k", Con 4))))

let _ =
  print_endline (test term0);
  print_endline (test term1);
  print_endline (test term2);
