open Core_kernel

(* Binomial approximation of an american call option *)

let american_call_price s k r sigma t steps =
  let f = exp (r *. (t /. float steps)) in
  let finv = 1. /. f in
  let u = exp (sigma *. sqrt (t /. float steps)) in
  let d = 1. /. u in
  let p_up = (f -. d) /. (u -. d) in
  let p_down = 1. -. p_up in
  let prices = Array.create 0. ~len:(steps+1) in
  prices.(0) <- s *. (d ** float steps);
  for i = 1 to steps do
    prices.(i) <- u *. u *. prices.(i-1)
  done;
  (* Calculate payoff at maturity *)
  let call_prices = Array.init (steps+1) ~f:(fun i ->
    Float.max 0. (prices.(i) -. k))
  in
  for j = steps-1 downto 0 do
    for i = 0 to j do
      call_prices.(i) <- finv *. (p_down *. call_prices.(i) +. p_up *. call_prices.(i+1));
      prices.(i) <- d *. prices.(i+1);
      (* Check for exercise *)
      call_prices.(i) <- Float.max call_prices.(i) (prices.(i) -. k)
    done;
  done;
  call_prices.(0)
;;

let s = 100.      (* price of underlying *)
let k = 100.      (* strike price *)
let r = 0.1       (* risk-free interest rate *)
let sigma = 0.25  (* volatility *)
let t = 1.0       (* time to maturity *)
let steps = 100   (* number of time steps *)

let _ =
  let call_price = american_call_price s k r sigma t steps in
  printf "American call price = %2.2f\n%!" call_price
;;
