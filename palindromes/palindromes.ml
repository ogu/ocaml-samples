open Core

(* Trie-based palindrome search. *)

module Trie : sig

  type t
  type word = char list

  val empty : t
  val insert : t -> word -> t
  val member : t -> word -> bool

  val of_words : word list -> t

end = struct

  type t = T of bool * arcs
  and arcs = (char, t) List.Assoc.t

  type word = char list

  let empty = T (false, [])

  let rec insert t word =
    match word, t with
    | [], T (_, arcs) -> T (true, arcs)
    | c :: cs, T (flag, arcs) ->
      let s =
        match List.Assoc.find arcs c ~equal:(=) with
        | None -> empty
        | Some s -> s
      in
      T (flag, (c, insert s cs) :: List.Assoc.remove arcs c ~equal:(=))

  let rec member t word =
    let T (flag, arcs) = t in
    match word with
    | [] -> flag
    | c :: cs ->
      match List.Assoc.find arcs c ~equal:(=) with
      | None -> false
      | Some s -> member s cs

  let of_words words =
    List.fold words ~init:empty ~f:insert
end

let explode s =
  let cs = ref [] in
  for i = String.length s - 1 downto 0 do
    cs := s.[i] :: !cs
  done;
  !cs

let implode l =
  let s = String.create (List.length l) in
  List.iteri l ~f:(fun i c -> s.[i] <- c);
  s

let palindromes word_list =
  let trie =
    word_list
    |> List.map ~f:explode
    |> Trie.of_words
  in
  List.filter word_list ~f:(fun word ->
    let rev_word = List.rev (explode word) in
    Trie.member trie rev_word)

let word_list =
  ["hannah"; "level"; "kinnikinnik"; "rotator"; "explosion"; "ocaml"]

let expected =
  ["hannah"; "level"; "kinnikinnik"; "rotator"]

let _ = assert (palindromes word_list = expected)
