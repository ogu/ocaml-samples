module Linked_list : sig

  module Elt : sig
    type 'a t
    val equal : 'a t -> 'a t -> bool
    val create : 'a -> 'a t
    val insert_before : 'a t -> 'a -> 'a t
    val unlink : 'a t -> unit

    val next : 'a t -> 'a t
    val prev : 'a t -> 'a t
    val value : 'a t -> 'a
  end

  type 'a t

  val create : unit -> 'a t
  val is_empty : 'a t -> bool

  val add : 'a t -> 'a -> 'a Elt.t
  val remove : 'a t -> 'a Elt.t -> unit

  val fold_elt : 'a t -> init:'b -> f:('b -> 'a Elt.t -> 'b) -> 'b
  val iter_elt : 'a t -> f:('a Elt.t -> unit) -> unit
end = struct

  module Elt = struct
    type 'a t = {
      value : 'a;
      mutable prev : 'a t;
      mutable next : 'a t;
    }

    let value t = t.value
    let prev  t = t.prev
    let next  t = t.next

    let equal = (==)

    let create x =
      let rec t = {
        value = x;
        prev  = t;
        next  = t;
      } in
      t

    let is_singleton t = equal t t.prev

    let insert_before t x =
      let node = {
        value = x;
        prev  = t.prev;
        next  = t;
      } in
      t.prev.next <- node;
      t.prev <- node;
      node

    let unlink t =
      if is_singleton t then
        ()
      else begin
        t.prev.next <- t.next;
        t.next.prev <- t.prev;
      end
  end

  type 'a t = 'a Elt.t option ref

  exception Elt_not_in_list

  let create () = ref None

  let is_empty t =
    match !t with
    | None   -> true
    | Some _ -> false

  let clear t = (t := None)

  let insert_empty t v =
    let new_elt = Elt.create v in
    t := Some new_elt;
    new_elt

  let add t v =
    match !t with
    | None -> insert_empty t v
    | Some first ->
      let new_elt = Elt.insert_before first v in
      t := Some new_elt;
      new_elt

  let remove t elt =
    match !t with
    | None -> raise Elt_not_in_list
    | Some first ->
      if Elt.is_singleton first && Elt.equal first elt then
        t := None
      else
        let rec loop cur =
          if Elt.equal elt cur then begin
            if Elt.equal cur first then
              t := Some (Elt.next first);
            Elt.unlink cur
          end else
            let next = Elt.next cur in
            loop next
        in
        loop (Elt.next first)

  let fold_elt t ~init ~f =
    match !t with
    | None -> init
    | Some first ->
      let rec loop elt acc =
        let acc = f acc elt in
        let next = Elt.next elt in
        if Elt.equal next first then acc else loop next acc
      in
      loop first init

  let iter_elt t ~f = fold_elt t ~init:() ~f:(fun () elt -> f elt)
end

module List = ListLabels

let () =
  let expected = [2;4;3;9;5;7;8;6;1] in
  let ll = Linked_list.create () in
  assert (Linked_list.is_empty ll);
  let elts = List.map expected ~f:(fun x -> Linked_list.add ll x) in
  assert (not (Linked_list.is_empty ll));
  let actual = Linked_list.fold_elt ll ~init:[] ~f:(fun acc elt ->
    Linked_list.Elt.value elt :: acc)
  in
  List.iter (List.rev elts) ~f:(fun elt -> Linked_list.remove ll elt);
  assert (Linked_list.is_empty ll);
  assert (expected = actual);
