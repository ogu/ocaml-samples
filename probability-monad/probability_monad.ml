open Core

(* The probability distribution monad from Ramsey & Pfeffer's paper
   `Stochastic lambda calculus and monads of probability distributions` *)
module Dist : sig

  type 'a t = private
    { (* A sample is a value from the distribution. *)
      sample : 'a;
      (* The support contains all samples with nonzero probability. *)
      support : 'a Set.Poly.t;
      (* [expectation h] computes the mean of [h] over the distribution. *)
      expectation : ('a -> float) -> float }

  include Monad.S with type 'a t := 'a t

  (* [choose p d1 d2] represents a linear combination of the two distributions
     [d1] and [d2]. *)
  val choose : float -> 'a t -> 'a t -> 'a t

  (* An extended version of [choose] to combine more than two weighted
     distributions. *)
  val dist : ('a * float) list -> 'a t

end = struct

  module M = struct
    type 'a t =
      { sample : 'a;
        support : 'a Set.Poly.t;
        expectation : ('a -> float) -> float; }

    let return x =
      { sample = x;
        support = Set.Poly.singleton x;
        expectation = fun h -> h x; }

    let bind d ~f =
      { sample = (f d.sample).sample;
        support =
          Set.Poly.map d.support ~f:(fun x -> (f x).support)
          |> Set.Poly.to_list
          |> Set.Poly.union_list;
        expectation = fun h -> d.expectation (fun x -> (f x).expectation h); }

    let map = `Custom (fun d ~f ->
      { sample = f d.sample;
        support = Set.Poly.map d.support ~f;
        expectation = fun h -> d.expectation (Fn.compose h f); })
  end
  include M
  include Monad.Make (M)

  let choose p d1 d2 =
    if p < 0. || p > 1. then failwith "choose: invalid probability";
    { sample = if Random.float 1. < p then d1.sample else d2.sample;
      support = Set.Poly.union d1.support d2.support;
      expectation = fun h -> p *. d1.expectation h +. (1.-.p) *. d2.expectation h; }

  let dist cases =
    let rec loop l q = match l with
      | [] -> failwith "dist: no cases"
      | [x, _] -> return x
      | (x, p) :: t -> choose (p /. q) (return x) (loop t (q -. p))
    in
    loop cases 1.
end
open Dist

(* The example model from Ramsey & Pfeffer's paper *)

let traffic_light = dist [`Red, 0.45; `Yellow, 0.1; `Green, 0.45]

let cautious_driver light = match light with
  | `Red    -> dist [`Braking, 0.2; `Stopped, 0.8]
  | `Yellow -> dist [`Braking, 0.9; `Driving, 0.1]
  | `Green  -> return (`Driving)

let aggressive_driver light = match light with
  | `Red    -> dist [`Braking, 0.3; `Stopped, 0.6; `Driving, 0.1]
  | `Yellow -> dist [`Braking, 0.1; `Driving, 0.9]
  | `Green  -> return (`Driving)

let other_light = function
  | `Red    -> `Green
  | `Yellow -> `Red
  | `Green  -> `Red

let crash driver1_dist driver2_dist light_dist =
  light_dist
  >>= fun light ->
  driver1_dist light
  >>= fun driver1 ->
  driver2_dist (other_light light)
  >>= fun driver2 ->
  match driver1, driver2 with
  | `Driving, `Driving -> dist [`Crash, 0.9; `No_crash, 0.1]
  | _ -> return (`No_crash)

let scenario : [`Crash | `No_crash] Dist.t =
  crash cautious_driver aggressive_driver traffic_light

(* We use the indicator function to compute the probability of a crash. *)
let crash_prob = scenario.expectation (function `Crash -> 1. | `No_crash -> 0.)

let _ = printf "The probability of a crash is %2.1f %%\n%!" (crash_prob *. 100.)
