open Core

(* Either remove the head of the list,
   or keep the head and remove an element from the tail. *)
let rec remove = function
  | [] | _ :: [] -> [[]]
  | x :: xs -> xs :: List.map (remove xs) ~f:(fun l -> x :: l)

let _ = assert (remove [] = [[]])
let _ = assert (remove [1] = [[]])
let _ = assert (remove [1;2;3] = [[2;3];[1;3];[1;2]])
let _ = assert (remove [1;2;3;4] = [[2;3;4];[1;3;4];[1;2;4];[1;2;3]])

(* Either insert the element at the beginning,
   or keep the head and insert it into the tail restoring the head afterwards. *)
let rec ndinsert x = function
  | [] -> [[x]]
  | y :: ys as l -> (x :: l) :: List.map (ndinsert x ys) ~f:(fun l -> y :: l)

let _ = assert (ndinsert 99 [] = [[99]])
let _ = assert (ndinsert 99 [1;2;3] = [[99;1;2;3];[1;99;2;3];[1;2;99;3];[1;2;3;99]])

(* Detach the head, find a permutation of the tail,
   put the detached head somewhere in the permuted list. *)
let rec permute = function
  | [] -> [[]]
  | x :: xs -> List.concat (List.map (permute xs) ~f:(ndinsert x))

let _ = assert (permute [1] = [[1]])
let _ = assert (permute [1;2;3] = [[1;2;3];[2;1;3];[2;3;1];
                                   [1;3;2];[3;1;2];[3;2;1]])

(* A monadic solution. *)
module Nondet : sig
  type 'a t

  val run : 'a t -> 'a
  val run_all : 'a t -> 'a list
  val fail : _ t
  val either : 'a t -> 'a t -> 'a t  (* choose *)

  include Monad.S with type 'a t := 'a t
end = struct
  module M = struct
    type 'a t = 'a list

    let return x = x :: []

    let rec bind m ~f =
      match m with
      | [] -> []
      | x :: xs -> List.append (f x) (bind xs f)

    let map = `Custom (fun t ~f -> List.map t ~f)
  end
  include M
  include (Monad.Make (M) : Monad.S with type 'a t := 'a t)

  let run m = List.hd_exn m
  let run_all m = m
  let fail = []
  let either a b = List.append a b
end

open Nondet

(* Either remove the head of the list,
   or keep the head and remove an element from the tail. *)
let rec m_remove = function
  | [] -> fail
  | h :: t ->
    either
      (return t)
      (m_remove t >>= fun t' -> return (h :: t'))

let _ = assert (run_all (m_remove []) = [])
let _ = assert (run_all (m_remove [1]) = [[]])
let _ = assert (run_all (m_remove [1;2;3]) = [[2;3];[1;3];[1;2]])
let _ = assert (run_all (m_remove [1;2;3;4]) = [[2;3;4];[1;3;4];[1;2;4];[1;2;3]])

(* Either insert the element at the beginning,
   or keep the head and insert it into the tail restoring the head afterwards. *)
let rec m_ndinsert x l =
  either
    (return (x :: l))
    (match l with
    | [] -> fail
    | h :: t -> m_ndinsert x t >>= fun t' -> return (h :: t'))

let _ = assert (run_all (m_ndinsert 99 []) = [[99]])
let _ = assert (run_all (m_ndinsert 99 [1;2;3])
                = [[99;1;2;3];[1;99;2;3];[1;2;99;3];[1;2;3;99]])

(* Detach the head, find a permutation of the tail,
   put the detached head somwhere in the permuted list. *)
let rec m_permute = function
  | [] -> return []
  | h :: t -> m_permute t >>= fun perm -> m_ndinsert h perm

let _ = assert (run_all (m_permute [1]) = [[1]])
let _ = assert (run_all (m_permute [1;2;3]) = [[1;2;3];[2;1;3];[2;3;1];
                                               [1;3;2];[3;1;2];[3;2;1]])
