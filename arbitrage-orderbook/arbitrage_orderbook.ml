open Core

module Side = struct
  type t = [ `Bid | `Ask ]
end

module Book_entry : sig
  type 'a t
  constraint 'a = [< Side.t ]

  val bid : price:float -> size:int -> [ `Bid ] t
  val ask : price:float -> size:int -> [ `Ask ] t

  val price : 'a t -> float
  val size  : 'a t -> int
  val decrease_size : 'a t -> by:int -> 'a t

  val compare : 'a t -> 'a t -> int
end =  struct

  type 'a t = { price : float; size : int }
  constraint 'a = [< Side.t ]

  let bid ~price ~size = { price; size }
  let ask ~price ~size = { price; size }

  let price t = t.price
  let size  t = t.size

  let decrease_size t ~by = { t with size = t.size - by }
  let compare t1 t2 = Float.compare t1.price t2.price
end

module Book_segment : sig
  type 'a t =
  | Empty
  | Partial of 'a Book_entry.t list * [ `Unfilled of int ]
  | Full of 'a Book_entry.t list
  constraint 'a = [< Side.t ]

  val empty : 'a t
  val partial : 'a Book_entry.t list -> int-> 'a t
  val full : 'a Book_entry.t list -> 'a t

  val entries   : 'a t -> 'a Book_entry.t list
  val avg_price : 'a t -> float
end = struct

  type 'a t =
  | Empty
  | Partial of 'a Book_entry.t list * [ `Unfilled of int ]
  | Full of 'a Book_entry.t list
  constraint 'a = [< Side.t ]

  let empty = Empty
  let partial entries size = Partial (entries, `Unfilled size)
  let full entries = Full entries

  let entries t = match t with
    | Empty -> []
    | Partial (entries, _) -> entries
    | Full entries -> entries

  let avg_price t =
    let entries = entries t in
    let total_size =
      List.map entries ~f:Book_entry.size
      |> List.fold ~init:0 ~f:(+)
      |> Int.to_float
    in
    List.fold entries ~init:0. ~f:(fun avg_price bid ->
      let size = float (Book_entry.size bid) in
      avg_price +. Book_entry.price bid *. size /. total_size)
end

module Arbitrage : sig
  type t (* Represents an arbitrage. *)

  val create
    :  size:int
    -> ask_price:float
    -> bid_price:float
    -> t

  val size  : t -> int
  val value : t -> float
end = struct

  type t = { size : int; ask_price : float; bid_price : float }

  let create ~size ~ask_price ~bid_price = { size; ask_price; bid_price }

  let size  t = t.size
  let value t = float t.size *. (t.bid_price -. t.ask_price)
end

module Arbitrage_orderbook : sig
  type t
  (* A naive order book implementation used to compute the size and value of an
     arbitrage.  An arbitrage exists whenever the bid-ask spread is crossed
     which is never the case in a real-world order book.  Hence this is just an
     artificial order book. *)

  val create
    :  bids: [ `Bid ] Book_entry.t list
    -> asks: [ `Ask ] Book_entry.t list
    -> t

  val extract_bids : t -> wanted_size:int -> [ `Bid ] Book_segment.t
  val extract_asks : t -> wanted_size:int -> [ `Ask ] Book_segment.t

  val detect_arbitrage : t -> Arbitrage.t option
end = struct

  type t = {
    bids : [ `Bid ] Book_entry.t list;
    asks : [ `Ask ] Book_entry.t list;
  }

  let create ~bids ~asks = {
    (* We sort the bids in descending order. *)
    bids = List.sort bids ~cmp:(fun x y -> Book_entry.compare y x);
    (* We sort the asks in ascending order. *)
    asks = List.sort asks ~cmp:(fun x y -> Book_entry.compare x y);
  }

  let extract_entries orders ~wanted_size =
    let rec loop orders extracted_orders total_size =
      match orders with
      | [] ->
        if List.is_empty extracted_orders then
          Book_segment.empty
        else
          let unfilled_size = wanted_size - total_size in
          Book_segment.partial (List.rev extracted_orders) unfilled_size
      | order :: rem_orders ->
        let total_size = total_size + (Book_entry.size order) in
        if total_size = wanted_size then
          Book_segment.full (order :: extracted_orders |> List.rev)
        else if total_size > wanted_size then
          let last_order =
            Book_entry.decrease_size order ~by:(total_size - wanted_size)
          in
          Book_segment.full (last_order :: extracted_orders |> List.rev)
        else
          loop rem_orders (order :: extracted_orders) total_size
    in
    loop orders [] 0

  let extract_bids t ~wanted_size = extract_entries t.bids ~wanted_size
  let extract_asks t ~wanted_size = extract_entries t.asks ~wanted_size

  let calc_arb_size ~bids ~asks =
    let rec loop bids asks arb_size =
      match bids, asks with
      | [], []
      | [], _
      | _ , [] -> arb_size
      | bid :: bids, ask :: asks ->
        let spread = Book_entry.price ask -. Book_entry.price bid in
        if spread >= 0. then arb_size
        else begin
          let bid_size = Book_entry.size bid in
          let ask_size = Book_entry.size ask in
          let exec_size = Int.min bid_size ask_size in
          let arb_size = arb_size + exec_size in
          if exec_size < bid_size then
            let unexec_bid = Book_entry.decrease_size bid ~by:exec_size in
            loop (unexec_bid :: bids) asks arb_size
          else if exec_size < ask_size then
            let unexec_ask = Book_entry.decrease_size ask ~by:exec_size in
            loop bids (unexec_ask :: asks) arb_size
          else
            loop bids asks arb_size
        end
    in
    loop bids asks 0

  let detect_arbitrage t =
    let arb_size = calc_arb_size ~bids:t.bids ~asks:t.asks in
    if arb_size = 0 then None else
      let ask_price = extract_asks t ~wanted_size:arb_size |> Book_segment.avg_price in
      let bid_price = extract_bids t ~wanted_size:arb_size |> Book_segment.avg_price in
      Some (Arbitrage.create ~size:arb_size ~ask_price ~bid_price)
end

(* Simple tests. *)

let entry_extraction_test () =
  let module A = Arbitrage_orderbook in
  let module S = Book_segment in
  let bid1 = Book_entry.bid ~size:200 ~price:12. in
  let bid2 = Book_entry.bid ~size:800 ~price:11. in
  let bid3 = Book_entry.bid ~size:700 ~price:10. in
  let bid4 = Book_entry.bid ~size:500 ~price:10. in
  let ask1 = Book_entry.ask ~size:700 ~price:12. in
  let ask2 = Book_entry.ask ~size:800 ~price:11. in
  let ask3 = Book_entry.ask ~size:200 ~price:10. in
  let ask4 = Book_entry.ask ~size:500 ~price:12. in
  let ob1 = A.create ~bids:[bid1;bid2;bid3] ~asks:[] in
  assert (A.extract_asks ob1 ~wanted_size:1000 |> S.entries = []);
  assert (A.extract_bids ob1 ~wanted_size:1000 |> S.entries = [bid1;bid2]);
  assert (A.extract_bids ob1 ~wanted_size:1700 |> S.entries = [bid1;bid2;bid3]);
  assert (A.extract_bids ob1 ~wanted_size:1500 |> S.entries = [bid1;bid2;bid4]);
  let ob2 = A.create ~bids:[] ~asks:[ask1;ask2;ask3] in
  assert (A.extract_bids ob2 ~wanted_size:1000 |> S.entries = []);
  assert (A.extract_asks ob2 ~wanted_size:1000 |> S.entries = [ask3;ask2]);
  assert (A.extract_asks ob2 ~wanted_size:1700 |> S.entries = [ask3;ask2;ask1]);
  assert (A.extract_asks ob2 ~wanted_size:1500 |> S.entries = [ask3;ask2;ask4])
;;

let no_arbitrage_test () =
  let arb_orderbook = Arbitrage_orderbook.create
    ~bids:[
      Book_entry.bid ~size:100 ~price:10.4;
      Book_entry.bid ~size:200 ~price:10.3;
    ]
    ~asks:[
      Book_entry.ask ~size:100 ~price:10.7;
      Book_entry.ask ~size:300 ~price:10.8;
    ]
  in
  assert (Option.is_none (Arbitrage_orderbook.detect_arbitrage arb_orderbook))
;;

let arbitrage_test () =
  let arb_orderbook = Arbitrage_orderbook.create
    ~bids:[
      Book_entry.bid ~size:100 ~price:10.4;
      Book_entry.bid ~size:200 ~price:10.3;
    ]
    ~asks:[
      Book_entry.ask ~size:100 ~price:10.1;
      Book_entry.ask ~size:300 ~price:10.2;
    ]
  in
  match Arbitrage_orderbook.detect_arbitrage arb_orderbook with
  | None -> assert false
  | Some arb ->
    assert (Arbitrage.size arb = 300);
    assert (Float.(Arbitrage.value arb =. 50.))
;;

let _ =
  entry_extraction_test ();
  no_arbitrage_test ();
  arbitrage_test ();
