module List = ListLabels

type pip =
| Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten
| Jack | Queen | King | Ace

let pips =
  [ Two; Three; Four; Five; Six; Seven; Eight; Nine; Ten;
    Jack; Queen; King; Ace ]

type suit = Diamonds | Spades | Hearts | Clubs
let suits = [ Diamonds; Spades; Hearts; Clubs ]

type card = pip * suit

let cartesian_product l1 l2 =
  if l2 = [] then [] else
    let rec loop l1 acc = match l1 with
      | [] -> acc
      | h :: t -> loop t (List.rev_append (List.map ~f:(fun x -> (h, x)) l2) acc)
    in
    List.rev (loop l1 [])

let full_deck = cartesian_product pips suits

(* Fisher-Yates shuffle
   http://en.wikipedia.org/wiki/Fisher-Yates_shuffle *)
let shuffle deck =
  let swap a i j =
    let temp = a.(i) in
    a.(i) <- a.(j);
    a.(j) <- temp
  in
  for i = Array.length deck - 1 downto 1 do
    let j = Random.int (i+1) in
    swap deck i j
  done

let shuffled_deck =
  let deck = Array.of_list full_deck in
  shuffle deck;
  Array.to_list deck
