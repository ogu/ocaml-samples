open Core

(* A naive implementation of Landin's SECD machine *)

type term =
| LIT of int
| VAR of name
| LAM of name * term
| APP of term * term
and name = string

type program = term

module Env = struct
  type ('name, 'value) t = ('name, 'value) List.Assoc.t
  let empty = []
  let extend t name value = (name, value) :: t
  let lookup t name = List.Assoc.find_exn t name ~equal:(=)
end

type value =
| INT of int
| PRIM of (value -> value)
| CLOSURE of name * term * env
and env = (name, value) Env.t

type stack = value list

type control = directive list
and directive =
| TERM of term
| APPLY

type dump =
| DUMP of stack * env * control * dump
| EMPTY

let rec step stack env control dump = match stack, env, control, dump with
  | a :: _, _, [], EMPTY ->
    a
  | a :: _, _, [], DUMP (s', e', c', d') ->
    step (a :: s') e' c' d'
  | s, e, TERM (LIT i) :: c, d ->
    step (INT i :: s) e c d
  | s, e, TERM (VAR n) :: c, d ->
    step (Env.lookup e n :: s) e c d
  | s, e, TERM (LAM (n, b)) :: c, d ->
    step (CLOSURE (n, b, e) :: s) e c d
  | s, e, TERM (APP (op, arg)) :: c, d ->
    step s e (TERM arg :: TERM op :: APPLY :: c) d
  | CLOSURE (n, b, e') :: a :: s, e, APPLY :: c, d ->
    step [] (Env.extend e' n a) [TERM b] (DUMP (s, e, c, d))
  | PRIM f :: a :: s, e, APPLY :: c, d ->
    step (f a :: s) e c d
  | _ -> failwith "step: wrong input"

let run terms =
  let succ = function
    | INT n -> INT (n + 1)
    | _ -> invalid_arg "succ: wrong input"
  in
  let init_env = Env.extend Env.empty "succ" (PRIM succ) in
  let init_control = List.map terms ~f:(fun t -> TERM t) in
  step [] init_env init_control EMPTY

let _ = assert(run [APP (LAM ("x", APP (VAR "succ", VAR "x")), LIT 1)] = INT 2)
