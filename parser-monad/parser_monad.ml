(* A simple parser monad. *)

open Core

module Conv : sig
  val explode : string -> char list
  val implode : char list -> string
end = struct
  let explode s =
    String.foldi s ~init:[] ~f:(fun i cs c -> c :: cs)
    |> List.rev

  let implode l =
    let s = String.create (List.length l) in
    List.iteri l ~f:(fun i c -> s.[i] <- c);
    s
end
open Conv

module Parser : sig
  type 'a t
  include Monad.S with type 'a t := 'a t

  val empty : _ list t

  val (|.) : 'a list t -> 'a list t -> 'a list t
  val (&.) : 'a list t -> 'a list t -> 'a list t
  val star : 'a list t -> 'a list t

  type word = char list
  val char : char -> word t

  val run : 'a t -> string -> ('a * word) option
end = struct
  module M = struct
    type 'a t = word -> ('a * word) option
    and word = char list

    let bind p ~f = fun word ->
      match p word with
      | Some (a, rem) -> let q = f a in q rem
      | None          -> None

    let return a = fun word -> Some (a, word)

    let map = `Custom (fun p ~f -> fun word ->
      match p word with
      | Some (a, rem) -> Some (f a, rem)
      | None          -> None)
  end
  include M
  include Monad.Make (M)

  let fail = fun _word -> None

  let empty = return []

  let ( |. ) p q = fun word ->
    match p word with
    | Some _ as x -> x
    | None        -> q word

  let ( &. ) p q =
    p >>= fun xs ->
    q >>= fun ys ->
    return (xs @ ys)

  let rec list p = many1 p |. empty
  and many1 p = p >>= fun x -> list p >>| fun xs -> x :: xs

  let star p = list p >>| List.concat

  let terminal ~f = fun word ->
    match word with
    | [] -> None
    | c :: cs -> if f c then Some ([c], cs) else None

  let char c = terminal ~f:((=) c)

  let run p s = p (explode s)
end
open Parser

(* Regular expression for a number in BNF.

   digit   ::= '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
   digits  ::= digit digit*
   optsign ::= '-' | '+' | empty
   optfrac ::= ( '.' digit* ) | empty
   optexp  ::= ( ( 'e' | 'E' ) optsign digits ) | empty
   number  ::= digits optfrac optexp
*)

let zero    = char '0'
let digit   = explode "123456789"
              |> List.map ~f:char
              |> List.fold ~init:zero ~f:(|.)
let digits  = digit &. star digit
let optsign = char '-' |. char '+' |. empty
let optfrac = (char '.' &. star digit) |. empty
let optexp  = ((char 'e' |. char 'E') &. optsign &. digits) |. empty
let number  = digits &. optfrac &. optexp

let expected = Some (['3'; '.'; '1'; '4'; 'e'; '-'; '1'; '2'], [])
let _ = assert (Parser.run number "3.14e-12" = expected)
