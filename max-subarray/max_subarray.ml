open Core

(* Solves the maximum subarray problem using Kadane's algorithm. *)

let kadane a =
  let n = Array.length a in
  let max_ending_here = ref 0. in
  let max_so_far = ref 0. in
  for i = 0 to n-1 do
    max_ending_here := Float.max 0. !max_ending_here +. a.(i);
    max_so_far := Float.max !max_so_far !max_ending_here
  done;
  !max_so_far

let () =
  assert (kadane [|10.|] = 10. );
  let test1 = [|-2.;-3.;4.;-1.;-2.;1.;5.;-3.|] in
  assert (kadane test1 = 7.);
  let test2 = [|-1.;1.;1.;-1.|] in
  assert (kadane test2 = 2.)

(* Note: Kadane's algorithm outputs zero when all numbers
   are negative. However, in this case we simply compute
   the maximum of the array to get the correct result. *)
let () =
  let a = [|-4.;-8.;-.2.;-7.|] in
  assert (kadane a = 0.);
  let get_max = Array.fold ~init:Float.neg_infinity ~f:Float.max in
  assert (get_max a = -2.)
