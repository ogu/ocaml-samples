open Core

module Graph : sig
  type t

  val from_adj_lists : int list list -> t
  (* Finds a path in a graph using breadth first search (BFS). *)
  val bfs_path : t -> src:int -> dst:int -> int list
end = struct

  type t =
    { num_nodes : int;
      adj_lists : int list array;
    }

  let create num_nodes =
    { num_nodes;
      adj_lists = Array.create [] ~len:num_nodes;
    }

  let add_edge t ~src ~dst =
    let adj_list = t.adj_lists.(src) in
    t.adj_lists.(src) <- dst :: adj_list

  let from_adj_lists adj_lists =
    let t = create (List.length adj_lists) in
    List.iteri adj_lists ~f:(fun u adj_list ->
      List.iter adj_list ~f:(fun v -> add_edge t ~src:u ~dst:v)
    );
    t

  let calc_bfs_path parents ~src ~dst =
    let rec loop path v =
      if v = src then path
      else
        match parents.(v) with
        | None -> []
        | Some u -> loop (u :: path) u
    in
    loop [dst] dst

  let bfs_path t ~src ~dst =
    let parents = Array.create None  ~len:t.num_nodes in
    let visited = Array.create false ~len:t.num_nodes in
    let queue = Queue.create () in
    visited.(src) <- true;
    Queue.enqueue queue src;
    while not (Queue.is_empty queue) do
      let u = Queue.dequeue_exn queue in
      let neighbors = t.adj_lists.(u) in
      List.iter neighbors ~f:(fun v ->
        if not visited.(v) then begin
          visited.(v) <- true;        (* mark current node as visited *)
          parents.(v) <- Some u;      (* update its parent *)
          Queue.enqueue queue v       (* and enqueue it *)
        end);
    done;
    calc_bfs_path parents ~src ~dst
end

let _ =
  (* The example graph is taken from Cormen et al. *)
  let g = Graph.from_adj_lists [
    (* node 0: *) [1; 4];
    (* node 1: *) [0; 5];
    (* node 2: *) [3; 5; 6];
    (* node 3: *) [2; 6; 7];
    (* node 4: *) [0];
    (* node 5: *) [1; 2; 6];
    (* node 6: *) [2; 3; 5];
    (* node 7: *) [3];
  ] in
  assert (Graph.bfs_path g ~src:4 ~dst:7 = [4; 0; 1; 5; 6; 3; 7]);
  assert (Graph.bfs_path g ~src:7 ~dst:4 = [7; 3; 6; 5; 1; 0; 4]);
;;
