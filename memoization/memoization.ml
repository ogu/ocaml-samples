open Core

(* A generic memoization combinator. *)
let memoize (f : ('a -> 'b) -> 'a -> 'b) =
  let table = Int.Table.create ~size:100 () in
  let rec func x =
    match Hashtbl.find table x with
    | Some res -> res
    | None ->
      let res = f func x  in
      Hashtbl.set table ~key:x ~data:res;
      res
  in
  func

(* Computes the nth Fibonacci number. *)
let fib = memoize (fun fib n -> if n <= 2 then 1 else fib (n-1) + fib (n-2))

let _ = assert (fib 1 = 1)
let _ = assert (fib 2 = 1)
let _ = assert (fib 30 = 832040)

(* Solution to the knapsack problem under the assumption
   that any number of each item is available. *)
type item = { weight : int; value : int }

let knapsack items = memoize (fun knapsack weight ->
  items
  |> List.filter ~f:(fun item -> item.weight <= weight)
  |> List.map ~f:(fun item -> item.value + knapsack (weight - item.weight))
  |> List.fold ~init:0 ~f:Int.max)

(* The following example is taken from Wikipedia:
   http://en.wikipedia.org/wiki/Knapsack_problem *)
let _ =
  let items = [ { weight = 12; value =  4 }
              ; { weight =  1; value =  2 }
              ; { weight =  4; value = 10 }
              ; { weight =  1; value =  1 }
              ; { weight =  2; value =  2 } ]
  in
  let max_weight = 15 in
  assert (knapsack items max_weight = 36)
