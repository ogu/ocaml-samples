(* Merge sort. *)

let rec split = function
  | [] | [_] as x -> (x, [])
  | x :: y :: l ->
    let (xs, ys) = split l in
    (x :: xs, y :: ys)

let _ = assert (split [1;2;3;4;5] = ([1;3;5], [2;4]))

let rec merge xs ys = match (xs, ys) with
  | (xs, []) -> xs
  | ([], ys) -> ys
  | (x :: xs, y :: ys) ->
    if x < y
    then x :: merge xs (y :: ys)
    else y :: merge (x :: xs) ys

let _ = assert (merge [1;3] [2;4] = [1;2;3;4])

let rec sort = function
  | [] | [_] as x -> x
  | xs ->
    let (us, vs) = split xs in
    merge (sort us) (sort vs)

let _ = assert (sort [3;4;1;2;5;7;6;9;8] = [1;2;3;4;5;6;7;8;9])

let make_rev_list n =
  let rec loop n k =
    if n = 0 then k []
    else loop (n - 1) (fun l -> k (n :: l))
  in
  loop n (fun x -> x)

let _ = assert (make_rev_list 5 = [5;4;3;2;1])

let test_with_stack_overflow () =
  let big_list = make_rev_list 1_000_000 in
  sort big_list

(* CPS version of merge sort. *)

let ($) f x = f x
let id x = x

let rec split_k l k = match l with
  | [] | [_] as x -> k (x, [])
  | x :: y :: l' ->
    split_k l' $ fun (xs, ys) ->
    k (x :: xs, y :: ys)

let _ = assert (split_k [1;2;3;4;5] id = ([1;3;5], [2;4]))

let rec merge_k xs ys k = match (xs, ys) with
  | (xs, []) -> k xs
  | ([], ys) -> k ys
  | (x :: xs, y :: ys) ->
    if x < y
    then merge_k xs (y :: ys) $ fun l -> k (x :: l)
    else merge_k (x :: xs) ys $ fun l -> k (y :: l)

let _ = assert (merge_k [1;3] [2;4] id = [1;2;3;4])

let rec sort_k l k = match l with
  | [] | [_] as x -> k x
  | xs ->
    split_k xs $ fun (us, vs) ->
    sort_k  us $ fun us_sorted ->
    sort_k  vs $ fun vs_sorted ->
    merge_k us_sorted vs_sorted k

let _ = assert (merge_k [1;3] [2;4] id = [1;2;3;4])
let _ = assert (sort_k [3;4;1;2;5;7;6;9;8] id = [1;2;3;4;5;6;7;8;9])

let test_without_stack_overflow () =
  let big_list = make_rev_list 1_000_000 in
  sort_k big_list id
